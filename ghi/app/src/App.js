import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import PresentationForm from './PresentationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />     //app component
          </Route>

          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>  

          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
            <Route path="attendees/new" element={<AttendConferenceForm />} />

          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>   
  

       </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
